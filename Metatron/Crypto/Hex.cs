﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;

namespace Metatron.Crypto
{
    public static class Hex
    {
        private static readonly uint[] _lookup32 = CreateLookup32();

        private static uint[] CreateLookup32()
        {
            var result = new uint[256];
            string s;

            for (int i = 0; i < 256; i++)
            {
                s = i.ToString("X2");
                result[i] = ((uint)s[0]) + ((uint)s[1] << 16);
            }

            return result;
        }

        public static string Encode(byte[] bytes)
        {
            char[] result = new char[bytes.Length * 2];

            for (int i = 0; i < bytes.Length; i++)
            {
                uint val = _lookup32[bytes[i]];

                result[2 * i] = (char)val;
                result[2 * i + 1] = (char)(val >> 16);
            }

            return new string(result);
        }

        public static byte[] Decode(string hex)
        {
            if (hex.Length % 2 == 1)
                throw new Exception("The binary key cannot have an odd number of digits");

            byte[] arr = new byte[hex.Length >> 1];

            for (int i = 0; i < hex.Length >> 1; ++i)
            {
                arr[i] = (byte)((GetHexVal(hex[i << 1]) << 4) + (GetHexVal(hex[(i << 1) + 1])));
            }

            return arr;
        }

        private static int GetHexVal(char hex)
        {
            int val = (int)hex;

            //For uppercase A-F letters:
            return val - (val < 58 ? 48 : 55);

            //For lowercase a-f letters:
            //return val - (val < 58 ? 48 : 87);

            //Or the two combined, but a bit slower:
            //return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
        }

        //public static string EncodeSlow(Byte[] bytes)
        //{
        //    return new SoapHexBinary(bytes).ToString();
        //}

        //public static Byte[] DecodeSlow(string hex)
        //{
        //    return SoapHexBinary.Parse(hex).Value;
        //}
    }
}
