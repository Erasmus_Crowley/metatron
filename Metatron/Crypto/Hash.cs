﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Security.Cryptography;
using System.Text;

namespace Metatron.Crypto
{
    public class Hash
    {
        private static SHA256CryptoServiceProvider sha = new SHA256CryptoServiceProvider();

        public static byte[] CalculateHash(string input)
        {
            return sha.ComputeHash(GetBytesFromString(input));
        }

        public static int CalculateHashCode(string input)
        {
            byte[] bytes = sha.ComputeHash(GetBytesFromString(input));

            //Console.WriteLine("Hash length: " + bytes.Length);

            return BitConverter.ToInt32(bytes, 0);
        }

        public static byte[] GetBytesFromString(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string GetStringFromBytes(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }
}
