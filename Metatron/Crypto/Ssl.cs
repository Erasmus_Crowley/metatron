﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Metatron.Crypto
{
    public class Ssl
    {
        public static X509Certificate2 GenerateSelfSignedCertificate(string name)
        {
            //ECDsa ecdsa = ECDsa.Create();
            X509Certificate2 cert;

            using (RSA rsa = RSA.Create(2048))
            {
                CertificateRequest request = new CertificateRequest("cn=" + name, rsa, HashAlgorithmName.SHA512, RSASignaturePadding.Pkcs1);
                request.CertificateExtensions.Add(new X509BasicConstraintsExtension(false, false, 0, false));

                request.CertificateExtensions.Add(
                new X509KeyUsageExtension(
                    X509KeyUsageFlags.DataEncipherment | 
                    X509KeyUsageFlags.KeyEncipherment | 
                    X509KeyUsageFlags.DigitalSignature, false));

                request.CertificateExtensions.Add(
                new X509EnhancedKeyUsageExtension(
                    new OidCollection
                    {
                    // server authentication
                    new Oid("1.3.6.1.5.5.7.3.1"),
                    },
                    false));

                cert = request.CreateSelfSigned(DateTimeOffset.Now.AddMinutes(-5), DateTimeOffset.Now.AddYears(5));
            }

            return new X509Certificate2(cert.Export(X509ContentType.Pfx, "WeNeedASaf3rPassword"), "WeNeedASaf3rPassword", X509KeyStorageFlags.MachineKeySet);

            //return cert;
        }
    }
}
