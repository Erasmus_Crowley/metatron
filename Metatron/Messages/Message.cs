﻿using Metatron.Connections;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Metatron
{
    [System.Serializable]
    public abstract class Message
    {
        public int ID { get; set; }

        public Message()
        {

            this.ID = MessageUtilities.CalculateMessageID(this.GetType());
        }

        public abstract void SerializeTo(BinaryWriter writer);
        public abstract void DeserializeFrom(BinaryReader reader);
    }
}
