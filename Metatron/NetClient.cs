﻿using Metatron.Connections;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Metatron
{
    public class NetClient
    {
        public delegate void ConnectSucceededEvent(NetClient client);
        public delegate void ConnectFailedEvent(NetClient client, string reason);
        public delegate void DisconnectedEvent(NetClient client);

        private bool _connectSuccessPending = false;
        public event ConnectSucceededEvent OnConnectSucceeded;

        private bool _connectFailurePending = false;
        private string _connectFailureReason = string.Empty;
        public event ConnectFailedEvent OnConnectFailed;

        private bool _disconnectPending = false;
        public event DisconnectedEvent OnDisconnected;

        public bool IsConnected { get; private set; }
        public bool SuppressDisconnectNotice { get; set; }

        private ThreadingMode _threading = ThreadingMode.Asynchronous;
        public ThreadingMode Threading
        {
            get
            {
                return _threading;
            }
            set
            {
                _threading = value;

                Protocol.Threading = value;
            }
        }

        private Protocol Protocol = new Protocol();

        private Connection connection;

        private string _address;
        private int _port;
        private bool _connectPending;

        public NetClient() : this(ThreadingMode.Asynchronous) { }
        public NetClient(ThreadingMode mode)
        {
            this.Threading = mode;
        }

        public void Connect(string host, int port)
        {
            this._address = host;
            this._port = port;

            Connect();
        }

        public void Connect()
        {
            if (!this._connectPending)
            {
                if (this._address != string.Empty)
                {
                    TcpClient client = new TcpClient()
                    {
                        NoDelay = true,
                    };
                    
                    this._connectPending = true;

                    try
                    {
                        client.BeginConnect(_address, _port, ConnectionComplete, client);
                    }
                    catch (SocketException ex)
                    {
                        if (ex.SocketErrorCode == SocketError.HostNotFound ||
                            ex.SocketErrorCode == SocketError.HostUnreachable)
                        { 
                            QueueConnectionFailureEvent("Unable to connect to server at that address.");

                            return;
                        }

                        throw (ex);
                    }

                    //Log.Write("Attempt to connect to " + address + ":" + port);
                }
                else
                {
                    QueueConnectionFailureEvent("Invalid address or port.");
                }
            }
        }

        private void ConnectionComplete(IAsyncResult result)
        {
            try
            {
                TcpClient client = (TcpClient)result.AsyncState;

                client.EndConnect(result);

                if (client.Connected)
                {
                    //Log.Write("Client is connected to " + client.Client.RemoteEndPoint + "!");
                    SslStream stream = new SslStream(client.GetStream(), false, new RemoteCertificateValidationCallback(ValidateServerCertificate), null);

                    if (CanAuthenticate(stream))
                    {
                        this.connection = new Connection(client, stream)
                        {
                            SharedProtocol = this.Protocol
                        };

                        this.connection.Interrupted += Interrupted;

                        this.IsConnected = true;

                        QueueConnectionSuccessEvent();
                    }
                }
                else
                {
                    QueueConnectionFailureEvent("Can't reach server.");
                }
            }
            catch (Exception)
            {
                //Log.Write(EventType.ERROR, "An error occured while connecting to server!");
                //Log.Write(ex);

                QueueConnectionFailureEvent("Can't reach server.");
            }
            finally
            {
                this._connectPending = false;
            }
        }


        public void Send(Message message)
        {
            if (!this.IsConnected)
            {
                Console.WriteLine("Unable to send message. Client is not connected.");
                return;
            }

            if (message == null)
            {
                Console.WriteLine("Unable to send message. Message is null.");
                return;
            }

            connection.Send(message);
        }

        public void AddMessageHandler(Type t, ProtocolMessageProxy.MessageReceivedEvent handler)
        {
            Protocol.MessageHandler(t).OnMessageReceived += handler;
        }

        public void RemoveMessageHandler(Type t, ProtocolMessageProxy.MessageReceivedEvent handler)
        {
            Protocol.MessageHandler(t).OnMessageReceived -= handler;
        }


        public void Disconnect()
        {
            this.connection.Interrupted -= this.Interrupted;
            this.connection.Dispose();

            this.IsConnected = false;

            QueueDisconnectionEvent();
            //Log.Write("Disconnected.");
        }



        #region Event Management

        private void QueueDisconnectionEvent()
        {
            _disconnectPending = true;
            UpdateAsynchronousEvents();
        }

        private void QueueConnectionSuccessEvent()
        {
            _connectSuccessPending = true;
            UpdateAsynchronousEvents();
        }

        private void QueueConnectionFailureEvent(string reason)
        {
            _connectFailureReason = reason;
            _connectFailurePending = true;
            UpdateAsynchronousEvents();
        }

        private void UpdateAsynchronousEvents()
        {
            if (this.Threading == ThreadingMode.Asynchronous)
            {
                RaiseQueuedEvents();
            }
        }

        //Method is called by the main thread of the owner of the client.
        //Raise the appropriate events for the clients in synchronous mode.
        public void UpdateSynchronousEvents()
        {
            if (this.Threading == ThreadingMode.Synchronous)
            {
                RaiseQueuedEvents();

                Protocol.UpdateSynchronousEvents();
            }
        }

        private void RaiseQueuedEvents()
        {
            if (this._connectFailurePending && this.OnConnectFailed != null)
            {
                this.OnConnectFailed(this, _connectFailureReason);
            }

            if (this._connectSuccessPending && this.OnConnectSucceeded != null)
            {
                this.OnConnectSucceeded(this);
            }

            if (this._disconnectPending && this.OnDisconnected != null)
            {
                this.OnDisconnected(this);
            }

            this._connectFailurePending = false;
            this._connectFailureReason = string.Empty;
            this._connectSuccessPending = false;
            this._disconnectPending = false;
        }

        private void Interrupted(Connection connection)
        {
            Disconnect();
        }

        #endregion

        #region SSL

        private Boolean CanAuthenticate(SslStream stream)
        {
            try
            {
                stream.AuthenticateAsClient("metatron");
                return true;
            }
            catch (AuthenticationException)
            {
                //Log.Write(EventType.ERROR, "An error occured while trying to authenticate as client.");
                //Log.Write(e);

                return false;
            }
            catch (Exception)
            {
                //Log.Write(EventType.ERROR, "An severe error occured while trying to authenticate as client.");
                //Log.Write(ex);

                return false;
            }
        }

        private static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            //The SSL cert is generated, so it will always throw a RemoteCertificateChainError. Ignore it.
            if (sslPolicyErrors == SslPolicyErrors.None || sslPolicyErrors == SslPolicyErrors.RemoteCertificateChainErrors)
            {
                //Cert is good.
                return true;
            }

            //Log.Write(EventType.ERROR, "Unable to validate SSL certifice from server.");
            //Log.Write(string.Format("Certificate error: {0}", sslPolicyErrors));

            foreach (var e in chain.ChainStatus)
            {
                //Log.Write(EventType.ERROR, "Chain: " + e.StatusInformation);
            }

            //Bad cert. Do not pass go.
            return false;
        }

        #endregion
    }
}
