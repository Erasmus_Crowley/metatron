﻿using Metatron.Crypto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Metatron.Connections
{
    public class MessageUtilities
    {
        public static int CalculateMessageID(Type type)
        {
            return Hash.CalculateHashCode(type.FullName);
        }
    }
}
