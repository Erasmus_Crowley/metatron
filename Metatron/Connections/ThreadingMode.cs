﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metatron.Connections
{
    public enum ThreadingMode
    {
        Synchronous,
        Asynchronous
    }
}
