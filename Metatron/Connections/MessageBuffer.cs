﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Metatron.Connections
{
    public class MessageBuffer
    {
        public delegate void MessageCompleteEvent(BinaryReader reader);
        public event MessageCompleteEvent MessageComplete;

        private MemoryStream buffer = new MemoryStream();
        private int expectedLength;

        private byte[] emptyMessageData = new byte[] { };

        private long UnprocessedBytes
        {
            get
            {
                return buffer.Length - buffer.Position;
            }
        }

        public void AppendData(byte[] data, int length)
        {
            buffer.Position = buffer.Length;
            buffer.Write(data, 0, length);
            buffer.Flush();
            buffer.Position = 0;

            CheckForCompletedMessage();
        }

        private void CheckForCompletedMessage()
        {
            if (UnprocessedBytes >= 2)
            {
                using (BinaryReader reader = new BinaryReader(buffer, new UTF8Encoding(), true))
                {
                    expectedLength = reader.ReadInt16();
                    buffer.Position -= 2;

                    if (UnprocessedBytes >= expectedLength)
                    {
                        byte[] data = reader.ReadBytes(expectedLength);

                        CompleteMessage(data);
                        DiscardProcessedData();
                    }
                }
            }
        }

        private void CompleteMessage(byte[] data)
        {
            if (this.MessageComplete != null)
            {
                using (MemoryStream ms = new MemoryStream(data))
                {
                    using (BinaryReader reader = new BinaryReader(ms, new UTF8Encoding(), true))
                    {
                        this.MessageComplete(reader);
                    }
                }
            }
        }

        private void DiscardProcessedData()
        {
            if (buffer.Position != 0)
            {
                MemoryStream newBuffer = new MemoryStream();
                newBuffer.SetLength(UnprocessedBytes);
                Buffer.BlockCopy(buffer.GetBuffer(), (int)buffer.Position, newBuffer.GetBuffer(), 0, (int)UnprocessedBytes);
                buffer = newBuffer;
            }
        }
    }
}
