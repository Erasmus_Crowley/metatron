﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Metatron.Connections
{
    public class NetworkListener
    {
        public delegate void NewConnectionEvent(Connection connection);

        public event NewConnectionEvent Connected;

        private int port;
        private X509Certificate cert;
        private TcpListener listener;
        bool running = false;

        public NetworkListener(int port, X509Certificate cert)
        {
            this.port = port;
            this.cert = cert;

            this.listener = new TcpListener(IPAddress.Any, port);
        }

        public void Start()
        {
            this.running = true;

            try
            {
                listener.Start();
                listener.BeginAcceptTcpClient(OnNewConnection, listener);
            }
            catch (Exception ex)
            {
                this.running = false;

                Console.WriteLine("An error occured while attempting to start listening for new connections.");
                Console.WriteLine(ex.Message);
                //Log.Write(ex);
            }
        }

        public void Stop()
        {
            this.running = false;

            try
            {
                listener.Stop();
            }
            catch (Exception)
            {
                Console.WriteLine("An error occured while attempting to stop listening for new connections.");
                //Log.Write(ex);
            }
        }

        private void OnNewConnection(IAsyncResult result)
        {
            try
            {
                TcpClient client = listener.EndAcceptTcpClient(result);
                client.NoDelay = true;

                SslStream stream = new SslStream(client.GetStream(), false);

                if (CanAuthenticate(stream))
                {
                    Connection conn = new Connection(client, stream);
                    ConnectionEstablished(conn);
                }
            }
            catch (ObjectDisposedException) { } //Server was shutdown. 
            catch (Exception)
            {
                Console.WriteLine("An error occured while accepting a new connection.");
                //Log.Write(ex);
            }
            finally
            {
                if (listener != null && this.running)
                {
                    listener.BeginAcceptTcpClient(OnNewConnection, listener);
                }
            }
        }

        private Boolean CanAuthenticate(SslStream stream)
        {
            try
            {
                stream.AuthenticateAsServer(cert, false, SslProtocols.Tls, false);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occured while negotiating SSL with a client.");
                Console.WriteLine(ex.ToString());

                return false;
            }
        }

        private void ConnectionEstablished(Connection connection)
        {
            if (this.Connected != null)
            {
                this.Connected(connection);
            }
        }

        public void Dispose()
        {
            cert = null;
            listener.Stop();
            listener = null;
        }
    }
}
