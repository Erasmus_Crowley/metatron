﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metatron.Connections
{
    public class SynchronousMessageContainer
    {
        public Connection Sender;
        public Message Message;

        public SynchronousMessageContainer(Connection sender, Message message)
        {
            this.Sender = sender;
            this.Message = message;
        }
    }
}
