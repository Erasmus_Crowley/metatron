﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;

namespace Metatron.Connections
{
    public class Connection : IDisposable
    {
        public delegate void InterruptedEvent(Connection connection);

        public event InterruptedEvent Interrupted;

        private TcpClient client;
        private SslStream stream;
        public Protocol SharedProtocol { get; set; }
        private Protocol PrivateProtocol = new Protocol();

        private MessageBuffer buffer;

        private ConcurrentQueue<Message> outgoing = new ConcurrentQueue<Message>();
        private bool sending = false;


        public Connection(TcpClient client, SslStream stream)
        {
            this.client = client;
            this.stream = stream;

            this.buffer = new MessageBuffer();
            buffer.MessageComplete += MessageComplete;

            byte[] data = new byte[1024];
            stream.BeginRead(data, 0, data.Length, ReadComplete, data);
        }

        public void Dispose()
        {
            this.stream.Dispose();
            this.stream = null;

            this.client = null;

            this.buffer.MessageComplete -= MessageComplete;
            this.buffer = null;
        }

        public void Send(Message message)
        {
            //Console.WriteLine("Connection queued: " + message.GetType().ToString() + " with ID: " + message.ID);

            outgoing.Enqueue(message);

            SendNextQueuedMessage();
        }

        private void SendNextQueuedMessage()
        {
            if (!sending && outgoing.Count > 0)
            {
                this.sending = true;

                if (outgoing.TryDequeue(out Message message))
                {
                    byte[] contents = SharedProtocol.SerializeMessage(message);
                    //Console.WriteLine("Sending: " + message.GetType().ToString() + " with ID: " + message.ID + ", size: " + contents.Length + " bytes.");

                    if (stream.CanWrite)
                    {
                        //Console.WriteLine("Sent!");
                        stream.BeginWrite(contents, 0, contents.Length, SendComplete, null);
                    }
                }
            }
        }

        public void AddMessageHandler(Type t, ProtocolMessageProxy.MessageReceivedEvent handler)
        {
            PrivateProtocol.MessageHandler(t).OnMessageReceived += handler;
        }

        public void RemoveMessageHandler(Type t, ProtocolMessageProxy.MessageReceivedEvent handler)
        {
            PrivateProtocol.MessageHandler(t).OnMessageReceived -= handler;
        }

        #region events

        private void ReadComplete(IAsyncResult result)
        {
            int read = 0;

            try
            {
                read = stream.EndRead(result);

                if (read > 0)
                {
                    //Console.WriteLine("Connection received " + read + " bytes.");
                    byte[] data = (byte[])result.AsyncState;
                    buffer.AppendData(data, read);

                    stream.BeginRead(data, 0, data.Length, ReadComplete, data);
                }
            }
            catch (Exception)
            {
                //Console.WriteLine("Error occured during ReadComplete.");
                //Console.WriteLine(ex.ToString());
            }

            if (read == 0)
            {
                //Console.WriteLine("Connection interrupted.");
                if (this.Interrupted != null)
                {
                    this.Interrupted(this);
                }
            }
        }

        private void SendComplete(IAsyncResult result)
        {
            try
            {
                stream.EndWrite(result);
                sending = false;

                //Console.WriteLine("Message sent successfully.");

                SendNextQueuedMessage();
            }
            catch (Exception)
            {
                //Console.WriteLine("Error occured during SendComplete.");
                //Console.Write(ex.ToString());
            }
        }

        private void MessageComplete(BinaryReader reader)
        {
            if (this.SharedProtocol != null)
                this.SharedProtocol.ProcessMessage(this, reader);

            if (this.PrivateProtocol != null)
                this.PrivateProtocol.ProcessMessage(this, reader);
        }


        #endregion
    }
}
