﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metatron.Connections
{
    public class ProtocolMessageProxy
    {
        public delegate void MessageReceivedEvent(Connection sender, Message message);
        public event MessageReceivedEvent OnMessageReceived;

        public Type MessageType { get; set; }

        public ProtocolMessageProxy(Type type)
        {
            this.MessageType = type;
        }

        public void ProcessMessage(Connection sender, Message message)
        {
            if (message != null && OnMessageReceived != null)
            {
                OnMessageReceived(sender, message);
            }
        }
    }
}
