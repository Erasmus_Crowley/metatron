﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;

namespace Metatron.Connections
{
    public class Protocol
    {
        private Dictionary<int, ProtocolMessageProxy> proxies = new Dictionary<int, ProtocolMessageProxy>();
        public ThreadingMode Threading = ThreadingMode.Asynchronous;

        private ConcurrentQueue<SynchronousMessageContainer> unprocessedMsgs = new ConcurrentQueue<SynchronousMessageContainer>();

        public Protocol() : this(ThreadingMode.Asynchronous) { }
        public Protocol(ThreadingMode mode)
        {
            this.Threading = mode;
        }

        public ProtocolMessageProxy MessageHandler(Type type)
        {
            int id = MessageUtilities.CalculateMessageID(type);


            if (!proxies.ContainsKey(id))
            {
                proxies.Add(id, new ProtocolMessageProxy(type));
                //Console.WriteLine("Registered event handler for message (" + type.FullName + ") with ID: " + id);
            }

            //Check for hash collisions on the proxy dictionary.
            if (proxies[id].MessageType != null && proxies[id].MessageType != type)
                throw new Exception("Messages " + proxies[id].MessageType.ToString() + " and " + type.ToString() + " have the same hash code. All message types must have type names that generate unique hash values!");

            return proxies[id];
        }

        public void ProcessMessage(Connection sender, BinaryReader reader)
        {
            Message message = DeserializeMessage(reader);

            if (message == null)
            {
                //Console.WriteLine("No handler registered for message with ID: " + message.ID);
                return;
            }

            if (Threading == ThreadingMode.Asynchronous)
            {
                proxies[message.ID].ProcessMessage(sender, message);
            }
            else
            {
                unprocessedMsgs.Enqueue(new SynchronousMessageContainer(sender, message));
            }
        }

        public void UpdateSynchronousEvents()
        {
            if (this.Threading == ThreadingMode.Synchronous)
            {
                while(unprocessedMsgs.Count > 0)
                {
                    if (unprocessedMsgs.TryDequeue(out SynchronousMessageContainer msg))
                        proxies[msg.Message.ID].ProcessMessage(msg.Sender, msg.Message);
                }
            }
        }

        public byte[] SerializeMessage(Message message)
        {
            try
            {
                using (MemoryStream buffer = new MemoryStream())
                {
                    using (BinaryWriter writer = new BinaryWriter(buffer))
                    {
                        writer.Write(Int16.MinValue);
                        writer.Write(message.ID);
                        message.SerializeTo(writer);

                        writer.Flush();

                        buffer.Position = 0;

                        if (buffer.Length <= UInt16.MaxValue)
                            writer.Write((UInt16)buffer.Length);
                        else
                            throw new Exception("Unable to send messages with length greater than " + UInt16.MaxValue + " bytes.");
                    }

                    return buffer.ToArray();
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Failed to serialize message.");
                return null;
            }
        }

        public Message DeserializeMessage(BinaryReader reader)
        {
            try
            {
                long pos = reader.BaseStream.Position;

                UInt16 length = reader.ReadUInt16();
                int id = reader.ReadInt32();

                if (!proxies.ContainsKey(id))
                {
                    //Console.WriteLine("No message handler registered for message ID: " + id.ToString());

                    reader.BaseStream.Position = pos;
                    return null;
                }

                //Message message = (Message)Activator.CreateInstance(events[id].MessageType);

                Message message = (Message)FormatterServices.GetUninitializedObject(proxies[id].MessageType);
                message.ID = id;

                message.DeserializeFrom(reader);

                reader.BaseStream.Position = pos;
                return message;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to deserialize message.");
                Console.WriteLine(ex.ToString());
                return null;
            }
        }
    }
}
