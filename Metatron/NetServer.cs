﻿using System;
using System.Collections.Generic;
using System.Text;
using Metatron.Connections;
using Metatron.Crypto;

namespace Metatron
{
    public class NetServer : IDisposable
    {
        public delegate void ClientConnectedEvent(Connection connection);
        public delegate void ClientDisconnectedEvent(Connection connection);

        public event ClientConnectedEvent OnClientConnected;
        public event ClientDisconnectedEvent OnClientDisconnected;

        public List<Connection> Connections = new List<Connection>();
        public bool Running { get; private set; }

        private NetworkListener listener;

        private Protocol Protocol = new Protocol();

        private int _port;
        public int Port
        {
            get
            {
                return _port;
            }
        }


        public NetServer(int port) : this(port, "metatron") { }

        public NetServer(int port, string sslstring)
        {
            this._port = port;
            this.listener = new NetworkListener(port, Ssl.GenerateSelfSignedCertificate(sslstring));
            listener.Connected += NewConnection;
        }

        public void Start()
        {
            listener.Start();

            this.Running = true;
        }

        public void Stop()
        {
            listener.Stop();

            foreach (Connection c in Connections)
            {
                c.Interrupted -= Interrupted;
                c.Dispose();
            }
            Connections.Clear();

            this.Running = false;
        }

        public void AddMessageHandler(Type t, ProtocolMessageProxy.MessageReceivedEvent handler)
        {
            Protocol.MessageHandler(t).OnMessageReceived += handler;
        }

        public void RemoveMessageHandler(Type t, ProtocolMessageProxy.MessageReceivedEvent handler)
        {
            Protocol.MessageHandler(t).OnMessageReceived -= handler;
        }

        public void Dispose()
        {
            Stop();

            listener.Connected -= NewConnection;
            listener.Dispose();
            listener = null;

            Connections = null;
        }

        #region events

        private void NewConnection(Connection connection)
        {
            this.Connections.Add(connection);
            connection.Interrupted += Interrupted;
            connection.SharedProtocol = this.Protocol;

            if (this.OnClientConnected != null)
                this.OnClientConnected(connection);
        }

        private void Interrupted(Connection connection)
        {
            this.Connections.Remove(connection);

            connection.Interrupted -= Interrupted;
            connection.Dispose();

            if (this.OnClientDisconnected != null)
                this.OnClientDisconnected(connection);
        }

        #endregion
    }
}
